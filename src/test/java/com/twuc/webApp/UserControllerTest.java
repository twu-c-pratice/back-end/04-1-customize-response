package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockMvcClientHttpRequestFactory;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void should_return_200_when_no_reponse() throws Exception {
        mockMvc.perform(get("/api/no-return-value")).andExpect(status().is(200));
    }

    @Test
    void should_return_204_when_using_response_status() throws Exception {
        mockMvc.perform(get("/api/no-return-value-with-annotation")).andExpect(status().is(204));
    }

    @Test
    void should_return_message() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/messages/hello"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
                .andExpect(MockMvcResultMatchers.content().string("hello"));
    }

    @Test
    void should_return_json() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/message-objects/hello"))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.content().string("{\"value\":\"hello\"}"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.value").value("hello"))
                .andExpect(MockMvcResultMatchers.status().is(200));

    }

    @Test
    void should_return_json_with_status_code() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/message-objects-with-annotation/hello"))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().is(202))
                .andExpect(MockMvcResultMatchers.jsonPath("$.value").value("hello"));
    }

    @Test
    void should_return_customs_response_entity() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/message-entities/hello"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.value").value("{message}"))
                .andExpect(header().string("X-Auth", "me"));
    }
}