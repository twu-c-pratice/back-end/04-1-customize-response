package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserController {

    @GetMapping("/no-return-value")
    public void getNoReturnValue(){
    }

    @GetMapping("/no-return-value-with-annotation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void getNoReturnValueWithAnnotation(){}

    @GetMapping("/messages/{message}")
    public String getMessage(@PathVariable String message){
        return message;
    }

    @GetMapping("/message-objects/{message}")
    public Message getMessageObject(@PathVariable String message){
        return new Message(message);
    }

    @GetMapping("/message-objects-with-annotation/{message}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Message getMessageObjectWithStatusCode(@PathVariable String message){
        return new Message(message);
    }

    @GetMapping("/message-entities/hello")
    public ResponseEntity getMessageWithResponseEntity(){
        return  ResponseEntity.status(200)
                .header("X-Auth", "me")
                .body(new Message("{message}"));
    }
}
